﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bill
{
    public class DbProxy
    {
        public static string ConnectionString
        {
            get
            {
                string connStr = ConfigurationManager.ConnectionStrings["BillDb_skolan"].ToString();

                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connStr);
                sb.ApplicationName = ApplicationName ?? sb.ApplicationName;
                sb.ConnectTimeout = (ConnectionTimeout > 0) ? ConnectionTimeout : sb.ConnectTimeout;

                return sb.ToString();
            }
        }

        public static IEnumerable<T> GetAll<T>()
        {
            SqlConnection conn = GetSqlConnection();
            var type = typeof(T);

            var command = conn.CreateCommand();
            command.CommandText = "select * from " + type.Name;

            var dataReader = command.ExecuteReader();
            var list = new List<T>();
            while (dataReader.Read())
            {
                var instance = (T)Activator.CreateInstance(type);
                foreach (var propertyInfo in type.GetProperties())
                {
                    propertyInfo.SetValue(instance, dataReader[propertyInfo.Name]);
                }
                list.Add(instance);
            }

            return list;
        }

        public static SqlConnection GetSqlConnection()
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            conn.Open();
            return conn;
        }

        public static int ConnectionTimeout { get; set; }

        /// <summary>
        /// Motsvarar "program_name" (select * from sys.dm_exec_sessions i SQL Manager)
        /// </summary>
        public static string ApplicationName
        {
            get;
            set;
        }
    }
}
