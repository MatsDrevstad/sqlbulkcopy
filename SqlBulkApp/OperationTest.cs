﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlBulkApp.Bill;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using SqlBulkApp.Models;

namespace SqlBulkApp
{
    class OperationTest
    {
        public static void CreateBulkCopyTest()
        {
            DbGenerate.DropTables();
            DbGenerate.CreateTables();
            InsertProductList();

            var proxy = new DbProxy();
            Console.WriteLine();
            Console.Write("Submit customers quantity: ");
            var count = int.Parse(Console.ReadLine());

            Console.Write("Submit bulksize?: ");
            int bulksize = int.Parse(Console.ReadLine());

            var customers = DbGenerate.CreateRandomCustomer(count);

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            proxy.AddCustomer(customers, 1);
            GenerateOrderProductTableListWithBULK(bulksize);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine("Insert Customers with BULK COPY: {0}ms", stopWatch.ElapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public static void GenerateOrderProductTableListWithBULK(int bulksize)
        {
            var proxy = new DbProxy();

            Random rnd = new Random();
            var listOfProduct = proxy.GetAll<Product>().ToList();
            var listOfCustomer = proxy.GetAll<Customer>().ToList();
            List<OrderProduct> listOfOrderProduct = new List<OrderProduct>();
            int countOrderNr = 0;

            var tempListOfOrderProduct = new List<OrderProduct>();

            foreach (var item in listOfCustomer)
            {

                // Ordrar per kund
                for (int i = 0; i < 10; i++)
                {
                    countOrderNr++;
                    var tempProductList = new List<Product>();

                    // Produkter per order
                    for (int j = 0; j < 5; j++)
                    {
                        int rndProductElement = rnd.Next(0, listOfProduct.Count - 1);

                        tempListOfOrderProduct.Add(new OrderProduct()
                        {
                            ProductName = listOfProduct[rndProductElement].ProductName,
                            CustomerId = item.Id,
                            UnitPrice = listOfProduct[rndProductElement].Price,
                            OrderNr = countOrderNr,
                            Qty = 1,
                        });
                    }

                }
                listOfOrderProduct.AddRange(tempListOfOrderProduct);
                tempListOfOrderProduct.Clear();
            }
            proxy.AddOrder(listOfOrderProduct, 1, bulksize);
        }

        internal static void UpdatePriceDB()
        {
            var proxy = new DbProxy();

            var stopWatch = new Stopwatch();

            Console.Write("Antal loopar: ");
            var input = Console.ReadLine();

            stopWatch.Start();
            var listOfProduct = proxy.GetAll<Product>().ToList();

            Console.WriteLine();
            for (int i = 0; i < int.Parse(input); i++)
            {
                foreach (var product in listOfProduct)
                {
                    UpdatePrice("1.25", product.ArticleId);
                    UpdatePrice("0.8", product.ArticleId);
                }
                Console.Write(".");
            }

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Successfully updated: " + stopWatch.ElapsedMilliseconds + " ms");
            Console.ForegroundColor = ConsoleColor.Gray;
            stopWatch.Stop();
        }

        private static void UpdatePrice(string price, int articleId)
        {
            var proxy = new DbProxy();
            proxy.Open();
            try
            {
                var sqlText = "UPDATE Product SET Price = Price * " + price + " WHERE ArticleId = " + articleId;

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                proxy.Close();
            }
        }

        public static void CreateSqlInsertTest()
        {
            DbGenerate.DropTables();
            DbGenerate.CreateTables();
            InsertProductList();
            var proxy = new DbProxy();

            Console.WriteLine();
            Console.Write("Submit desired customers quantity: ");
            var count = int.Parse(Console.ReadLine());
            var customers = DbGenerate.CreateRandomCustomer(count);

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            proxy.AddCustomer(customers, 0);
            GenerateOrderProductTableListWithSQLINSERT();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine("Insert Customers with SQL INSERT: {0}ms", stopWatch.ElapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;

        }

        public static void GenerateOrderProductTableListWithSQLINSERT()
        {
            var proxy = new DbProxy();            

            Random rnd = new Random();
            var listOfProduct = proxy.GetAll<Product>().ToList();
            var listOfCustomer = proxy.GetAll<Customer>().ToList();
            List<OrderProduct> listOfOrderProduct = new List<OrderProduct>();
            int countOrderNr = 0;

            foreach (var item in listOfCustomer)
            {
                var tempListOfOrderProduct = new List<OrderProduct>();

                // Ordrar per kund
                for (int i = 0; i < 10; i++)
                {
                    countOrderNr++;
                    var tempProductList = new List<Product>();

                    // Produkter per order
                    for (int j = 0; j < 5; j++)
                    {
                        int rndProductElement = rnd.Next(0, listOfProduct.Count - 1);

                        tempListOfOrderProduct.Add(new OrderProduct()
                        {
                            ProductName = listOfProduct[rndProductElement].ProductName,
                            CustomerId = item.Id,
                            UnitPrice = listOfProduct[rndProductElement].Price,
                            OrderNr = countOrderNr,
                            Qty = 1,
                        });
                    }

                }
                proxy.AddOrder(tempListOfOrderProduct, 0);
            }
        }

        public static void InsertProductList()
        {
            var proxy = new DbProxy(); 
            IEnumerable<Product> listOfProducts = ListofProducts.Products;

            var bulkCopy = new SqlBulkCopy(proxy.GetConnection());
            
            bulkCopy.DestinationTableName = "Product";
            bulkCopy.ColumnMappings.Add("ProductName", "ProductName");
            bulkCopy.ColumnMappings.Add("Price", "Price");
            bulkCopy.ColumnMappings.Add("Description", "Description");
            bulkCopy.ColumnMappings.Add("ArticleId", "ArticleId");

            proxy.Open();
            try
            {
                using (var datareader = new ObjectDataReader<Product>(listOfProducts))
                {
                    bulkCopy.WriteToServer(datareader);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }

        internal static void CreateInvoice()
        {
            var proxy = new DbProxy();

            var currentInvoiceNr = 1;
            try
            {
                currentInvoiceNr = proxy.GetAll<Invoice>().Max(p => p.InvoiceNr);
                currentInvoiceNr++;
            }
            catch { }
            Console.WriteLine("Current Invoice nr: " + (currentInvoiceNr - 1));

            proxy.Close();

            var result = proxy.GetAll<OrderProduct>().GroupBy(p => p.OrderNr).Select(q => q.First()).Where(r => r.Status == 0);
            Console.WriteLine("Nr of unmade invoices: " + result.Count());

            proxy.Open();
            var rawInvoice = new List<Invoice>();

            try
            {
                string sqlText = @"
                    SELECT op.Id AS opid, op.*, cu.*
                    FROM OrderProduct op
                    INNER JOIN Customer cu
                    ON op.CustomerId = cu.Id
                    WHERE op.Status = 0
                    ";

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        rawInvoice.Add(new Invoice
                        {
                            InvoiceNr = 0, // currentInvoiceNr++ sätts senare pga att vi kör en group på denna lista
                            OrderNr = (int)dataReader["OrderNr"],
                            FullName = (string)dataReader["FirstName"] + " " + (string)dataReader["LastName"],
                            Address = (string)dataReader["Address"],
                            City = (string)dataReader["City"],
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }

            var listInvoice = new List<Invoice>();
            foreach (var item in rawInvoice.GroupBy(p => p.OrderNr).Select(q => q.First()))
            {
                listInvoice.Add(item);
                item.InvoiceNr = currentInvoiceNr++;
            }

            int bulksize = 10000;
            
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            proxy.Open();
            try
            {
                var bulkCopy = new SqlBulkCopy(proxy.GetConnection());
                bulkCopy.DestinationTableName = "Invoice";
                bulkCopy.ColumnMappings.Add("InvoiceNr", "InvoiceNr");
                bulkCopy.ColumnMappings.Add("OrderNr", "OrderNr");
                bulkCopy.ColumnMappings.Add("FullName", "FullName");
                bulkCopy.ColumnMappings.Add("Address", "Address");
                bulkCopy.ColumnMappings.Add("City", "City");

                do
                {
                    using (var dataReader = new ObjectDataReader<Invoice>(listInvoice.Take(bulksize)))
                    {
                        bulkCopy.WriteToServer(dataReader);
                    }
                    IEnumerable<Invoice> enumListOrder = listInvoice;
                    listInvoice = enumListOrder.Skip(bulksize).ToList();

                } while (listInvoice.Count() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Insert successfully in {0}ms", stopwatch.ElapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;
            stopwatch.Stop();

            proxy.Open();
            try
            {
                string sqlText = "UPDATE OrderProduct SET Status = 1 where Status = 0";

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }
    }
}
