﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SqlBulkApp.Bill
{

    public class DbProxy
    {
        private SqlConnection _connection;

        public DbProxy()
        {
            _connection = new SqlConnection(ConnectionString());
        }

        public void Open()
        {
            _connection.Open();
        }

        public void Close()
        {
            _connection.Close();
        }

        public SqlConnection GetConnection()
        {
            return _connection;
        }

        private string ConnectionString()
        {
            string connStr = ConfigurationManager.ConnectionStrings["BillDb_skolan"].ToString();
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connStr);

            sb.ApplicationName = "ConsoleAdoInvoice";
            sb.ConnectTimeout = 30;

            return sb.ToString();
        }

        public IEnumerable<T> GetAll<T>(string view = "")
        {
            var type = typeof(T);

            var command = _connection.CreateCommand();
            command.CommandText = string.IsNullOrEmpty(view) ? "SELECT * FROM " + type.Name : command.CommandText = "SELECT * FROM [" + view + "]";
            _connection.Open();
            var dataReader = command.ExecuteReader();
            var list = new List<T>();
            while (dataReader.Read())
            {
                var instance = (T)Activator.CreateInstance(type);
                foreach (PropertyInfo propertyInfo in type.GetProperties())
                {
                    ExcludeNull<T>(dataReader, instance, propertyInfo);
                }
                list.Add(instance);
            }
            _connection.Close();
            return list;
        }

        public IEnumerable<T> GetById<T>(string id)
        {
            var type = typeof(T);

            var command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM " + type.Name + " WHERE Id = " + id;
            _connection.Open();
            var dataReader = command.ExecuteReader();
            var list = new List<T>();
            while (dataReader.Read())
            {
                var instance = (T)Activator.CreateInstance(type);
                foreach (var propertyInfo in type.GetProperties())
                {
                    ExcludeNull<T>(dataReader, instance, propertyInfo);
                }
                list.Add(instance);
            }
            _connection.Close();
            return list;
        }

        private static void ExcludeNull<T>(SqlDataReader dataReader, T instance, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.Equals(typeof(string)))
            {
                if (dataReader[propertyInfo.Name].ToString().Equals(string.Empty))
                { propertyInfo.SetValue(instance, string.Empty); }
                else
                { propertyInfo.SetValue(instance, dataReader[propertyInfo.Name]); }
            }
            else if (propertyInfo.PropertyType.Equals(typeof(int)))
            {
                if (dataReader[propertyInfo.Name].ToString().Equals(string.Empty))
                { propertyInfo.SetValue(instance, 0); }
                else
                { propertyInfo.SetValue(instance, dataReader[propertyInfo.Name]); }
            }
            else
            {
                propertyInfo.SetValue(instance, dataReader[propertyInfo.Name]);
            }
        }

        /// <summary>
        /// method 0 plain sql, method 1 SqlBulkApp
        /// </summary>
        public void AddOrder(IEnumerable<OrderProduct> orderproduktlist, int method, int bulksize = 10000)
        {
            switch (method)
            {
                case 0:
                    _connection.Open();
                    var command = _connection.CreateCommand();
                    command.CommandText = "INSERT INTO OrderProduct (CustomerId, OrderNr, ProductName, UnitPrice, Status, Qty) VALUES (@CustomerId, @OrderNr, @ProductName, @UnitPrice, @Status, @Qty)";
                    var customeridParam = command.Parameters.Add("@CustomerId", SqlDbType.Int);
                    var ordernrParam = command.Parameters.Add("@OrderNr", SqlDbType.Int);
                    var productnameParam = command.Parameters.Add("@ProductName", SqlDbType.NVarChar);
                    var unitpriceParam = command.Parameters.Add("@UnitPrice", SqlDbType.Money);
                    var statusParam = command.Parameters.Add("@Status", SqlDbType.Int);
                    var qtyParam = command.Parameters.Add("@Qty", SqlDbType.Int);

                    foreach (var orderproduct in orderproduktlist)
                    {
                        customeridParam.Value = orderproduct.CustomerId;
                        ordernrParam.Value = orderproduct.OrderNr;
                        productnameParam.Value = orderproduct.ProductName;
                        unitpriceParam.Value = orderproduct.UnitPrice;
                        statusParam.Value = orderproduct.Status;
                        qtyParam.Value = orderproduct.Qty;
                        command.ExecuteNonQuery();
                    }
                    _connection.Close();
                    break;

                case 1:
                    _connection.Open();
                    var bulkCopy = new SqlBulkCopy(_connection);
                    bulkCopy.DestinationTableName = "OrderProduct";
                    bulkCopy.ColumnMappings.Add("CustomerId", "CustomerId");
                    bulkCopy.ColumnMappings.Add("OrderNr", "OrderNr");
                    bulkCopy.ColumnMappings.Add("ProductName", "ProductName");
                    bulkCopy.ColumnMappings.Add("UnitPrice", "UnitPrice");
                    bulkCopy.ColumnMappings.Add("Status", "Status");
                    bulkCopy.ColumnMappings.Add("Qty", "Qty");

                    do
                    {
                        using (var dataReader = new ObjectDataReader<OrderProduct>(orderproduktlist.Take(bulksize)))
                        {
                            bulkCopy.WriteToServer(dataReader);
                        }
                        orderproduktlist = orderproduktlist.Skip(bulksize);

                    } while (orderproduktlist.Count() > 0);
                    _connection.Close();
                    break;
            }
        }

        /// <summary>
        /// method 0 plain sql, method 1 SqlBulkApp
        /// </summary>
        public void AddCustomer(IEnumerable<Customer> customer, int method)
        {
            switch (method)
            {
                case 0:
                    _connection.Open();
                    var command = _connection.CreateCommand();
                    command.CommandText = "INSERT INTO Customer (FirstName, LastName, Address, City) VALUES (@FirstName, @LastName, @Address, @City)";
                    var firstnameParam = command.Parameters.Add("@FirstName", SqlDbType.NVarChar);
                    var lastnameParam = command.Parameters.Add("@LastName", SqlDbType.NVarChar);
                    var addressParam = command.Parameters.Add("@Address", SqlDbType.NVarChar);
                    var cityParam = command.Parameters.Add("@City", SqlDbType.NVarChar);

                    foreach (var customers in customer)
                    {
                        firstnameParam.Value = customers.FirstName;
                        lastnameParam.Value = customers.LastName;
                        addressParam.Value = customers.Address;
                        cityParam.Value = customers.City;
                        command.ExecuteNonQuery();
                    }
                    _connection.Close();
                    break;

                case 1:
                    _connection.Open();
                    var bulkCopy = new SqlBulkCopy(_connection);
                    bulkCopy.DestinationTableName = "Customer";
                    bulkCopy.ColumnMappings.Add("FirstName", "FirstName");
                    bulkCopy.ColumnMappings.Add("LastName", "LastName");
                    bulkCopy.ColumnMappings.Add("Address", "Address");
                    bulkCopy.ColumnMappings.Add("City", "City");

                    using (var dataReader = new ObjectDataReader<Customer>(customer))
                    {
                        bulkCopy.WriteToServer(dataReader);
                    }
                    _connection.Close();
                    break;
            }
        }
    }
}
    
    
    
