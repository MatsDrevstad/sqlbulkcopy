﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlBulkApp.Bill;
using System.Diagnostics;
using System.Data;
using System.Text.RegularExpressions;

namespace SqlBulkApp
{
    class DbGenerate
    {
        public static void CreateTables()
        {
            
            bool loop = true;

            while (loop)
	        {
                Console.Write(Globals.SqlType + " mode. Want to swith? [y/n] ");
                if (Console.ReadLine().Equals("y"))
                {
                    Globals.SqlType = Globals.SwitchSqlType();
                }
                else { loop = false; }
	        }

            var proxy = new DbProxy();

            try
            {
                SqlCommand command1 = new SqlCommand(Globals.ConfirmSqlType(DbDesign.createTableCustomer), proxy.GetConnection());
                SqlCommand command2 = new SqlCommand(Globals.ConfirmSqlType(DbDesign.createTableOrderProducts), proxy.GetConnection());
                SqlCommand command3 = new SqlCommand(Globals.ConfirmSqlType(DbDesign.createTableProduct), proxy.GetConnection());
                SqlCommand command4 = new SqlCommand(Globals.ConfirmSqlType(DbDesign.createTableInvoice), proxy.GetConnection());
                SqlCommand command5 = new SqlCommand(DbDesign.createGetProductList, proxy.GetConnection());
                SqlCommand command6 = new SqlCommand(DbDesign.createProductView, proxy.GetConnection());
                SqlCommand command7 = new SqlCommand(DbDesign.createSpAddProduct, proxy.GetConnection());

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                proxy.Open();
                command1.ExecuteNonQuery();
                command2.ExecuteNonQuery();
                command3.ExecuteNonQuery();
                command4.ExecuteNonQuery();
                command5.ExecuteNonQuery();
                command6.ExecuteNonQuery();
                command7.ExecuteNonQuery();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(); 
                Console.WriteLine("Created successfully {0}ms", stopwatch.ElapsedMilliseconds);
                Console.ForegroundColor = ConsoleColor.Gray;
                stopwatch.Stop();

            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }

        public static void DropKeys()
        {
            var proxy = new DbProxy();

            try
            {
                SqlCommand command1 = new SqlCommand(DbDesign.dropKeyOrderProducts, proxy.GetConnection());

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                
                proxy.Open();
                command1.ExecuteNonQuery();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine("Altered successfully {0}ms", stopwatch.ElapsedMilliseconds);
                Console.ForegroundColor = ConsoleColor.Gray;
                stopwatch.Stop();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }

        public static void CreateKeys()
        {
            var proxy = new DbProxy();

            try
            {
                SqlCommand command1 = new SqlCommand(DbDesign.createKeyOrderProducts, proxy.GetConnection());

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                proxy.Open();
                command1.ExecuteNonQuery();

                Console.ForegroundColor = ConsoleColor.Gray;
                stopwatch.Stop();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine("Altered successfully {0}ms", stopwatch.ElapsedMilliseconds);
                Console.ForegroundColor = ConsoleColor.Gray;
                stopwatch.Stop();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }

        public static void DropTables()
        {
            var proxy = new DbProxy();
            try
            {
                SqlCommand command1 = new SqlCommand(DbDesign.dropTableOrderProducts, proxy.GetConnection());
                SqlCommand command2 = new SqlCommand(DbDesign.dropTableCustomer, proxy.GetConnection());
                SqlCommand command3 = new SqlCommand(DbDesign.dropTableProduct, proxy.GetConnection());
                SqlCommand command4 = new SqlCommand(DbDesign.dropTableInvoice, proxy.GetConnection());
                SqlCommand command5 = new SqlCommand(DbDesign.dropGetProductList, proxy.GetConnection());
                SqlCommand command6 = new SqlCommand(DbDesign.dropProductView, proxy.GetConnection());
                SqlCommand command7 = new SqlCommand(DbDesign.dropSpAddProduct, proxy.GetConnection());

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                proxy.Open();
                command1.ExecuteNonQuery();
                command2.ExecuteNonQuery();
                command3.ExecuteNonQuery();
                command4.ExecuteNonQuery();
                command5.ExecuteNonQuery();
                command6.ExecuteNonQuery();
                command7.ExecuteNonQuery();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine("Deleted successfully {0}ms", stopwatch.ElapsedMilliseconds);
                Console.ForegroundColor = ConsoleColor.Gray;
                stopwatch.Stop();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }


        public static void GenerateOrderProductTable()
        {
            var proxy = new DbProxy();
            Random rnd = new Random();
            var listOfProduct = proxy.GetAll<Product>().ToList();
            var listOfCustomer = proxy.GetAll<Customer>().ToList();
            List<OrderProduct> listOfOrderProduct = new List<OrderProduct>();
            int countOrderNr = 0;

            var tempListOfOrderProduct = new List<OrderProduct>();

            foreach (var item in listOfCustomer)
            {
                // Ordrar per kund
                for (int i = 0; i < 10; i++)
                {
                    countOrderNr++;
                    var tempProductList = new List<Product>();

                    // Produkter per order
                    for (int j = 0; j < 5; j++)
                    {
                        int rndProductElement = rnd.Next(0, listOfProduct.Count - 1);

                        tempListOfOrderProduct.Add(new OrderProduct()
                        {
                            ProductName = listOfProduct[rndProductElement].ProductName,
                            CustomerId = item.Id,
                            UnitPrice = listOfProduct[rndProductElement].Price,
                            OrderNr = countOrderNr,
                            Status = 0,
                            Qty = 1,
                        });
                    }

                }
            }
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            proxy.AddOrder(tempListOfOrderProduct, 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Insert successfully in {0}ms", stopwatch.ElapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;
            stopwatch.Stop();
        }

        public static void PopulateAllTables()
        {
            Console.WriteLine();
            Console.Write("Enter the number of customers: ");
            var peopleCount = int.Parse(Console.ReadLine());
            var customer = CreateRandomCustomer(peopleCount);

            var proxy = new DbProxy();

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            proxy.AddCustomer(customer, 1);
            OperationTest.InsertProductList();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Updated successfully {0}ms", stopwatch.ElapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;
            stopwatch.Stop();
        }

        public static IEnumerable<Customer> CreateRandomCustomer(int count)
        {
            Random rnd = new Random();
            var randFirst = RandomFirstName();
            var randLast = RandomLastName();
            var randAddress = RandomAddress();
            var randCity = RandomCity();

            return Enumerable.Range(0, count)
                .Select(i => new Customer
                {
                    FirstName = randFirst[rnd.Next(0, randFirst.Count)],
                    LastName = randLast[rnd.Next(0, randLast.Count)],
                    Address = randAddress[rnd.Next(0, randAddress.Count)],
                    City = randCity[rnd.Next(0, randCity.Count)]
                });
        }


        #region Name Generator

        public static List<string> RandomFirstName()
        {
            return new List<string> { "Peter", "Lisa", "Gustav", "Erika", "Mats", "Olof", "Jessica", "Filippa", "Sofia", "Lars" };
        }

        public static List<string> RandomLastName()
        {
            return new List<string> { "Andersson", "Nilsson", "Johansson", "Olofsson", "Persson", "Lundsbrunn", "Falk", "Beck", "Wallander", "Henriksson" };
        }

        public static List<string> RandomAddress()
        {
            return new List<string> { "Lantgårdsg", "Idrottsv", "Gårdstorget", "Brogatan", "Göteborgsv", "Silverregnsv", "Hästhovsg", "Rubing", "Dödskalleg", "Kastanjev" };
        }

        public static List<string> RandomCity()
        {
            return new List<string> { "Göteborg", "Stockholm", "Malmö", "Kiruna", "Trollhättan", "Karlstad", "Jönköping", "Gävle", "Mora", "Uppsala" };
        }


        #endregion

        internal static void AlterCustomer()
        {
            bool loop = true;
            while (loop) { loop = AlterCustomerProcess(); }
        }

        private static bool AlterCustomerProcess()
        {
            var proxy = new DbProxy();
            var countFullNameNulls = proxy.GetAll<Customer>().Where(x => string.IsNullOrEmpty(x.FullName)).Count();
            var countFirstNameNulls = proxy.GetAll<Customer>().Where(x => string.IsNullOrEmpty(x.LastName)).Count();

            Console.WriteLine();
            Console.WriteLine("FullName contains no data in {0} rows", countFullNameNulls);
            Console.WriteLine("FirstName (and LastName) contains no data in {0} rows", countFirstNameNulls);

            Console.Write("Toggle [y/n] ");
            var toggle = Console.ReadLine();

            if (Regex.IsMatch(toggle, @"^y+$"))
            {
                if (countFullNameNulls > 0)
	            {
                    string sqlText1 = "UPDATE Customer SET FullName = CONCAT (FirstName, ' ', LastName)";
                    string sqlText2 = "UPDATE Customer SET FirstName = '', LastName = ''";

                    try
                    {
                        SqlCommand command1 = new SqlCommand(sqlText1, proxy.GetConnection());
                        SqlCommand command2 = new SqlCommand(sqlText2, proxy.GetConnection());

                        var stopwatch = new Stopwatch();
                        stopwatch.Start();

                        proxy.Open();
                        command1.ExecuteNonQuery();
                        command2.ExecuteNonQuery();

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine();
                        Console.WriteLine("Updated successfully {0}ms", stopwatch.ElapsedMilliseconds);
                        Console.ForegroundColor = ConsoleColor.Gray;
                        stopwatch.Stop();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return true;
                    }
                    finally
                    {
                        proxy.Close();
                    }
                    return true;
                }
                else if (countFirstNameNulls > 0)
	            {
                    var allCustomer = proxy.GetAll<Customer>();

                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    proxy.Open();
                    foreach (var item in allCustomer)
                    {
                        string sqlText1 = "UPDATE Customer SET FirstName = '" + item.FullName.Split(' ')[0] + 
                            "', LastName = '" + item.FullName.Split(' ')[1] +
                            "' WHERE Id = " + item.Id;
                        try
                        {
                            SqlCommand command1 = new SqlCommand(sqlText1, proxy.GetConnection());
                            command1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    proxy.Close();

                    string sqlText2 = "UPDATE Customer SET FullName = ''";
                    proxy.Open();

                    try
                    {
                        SqlCommand command1 = new SqlCommand(sqlText2, proxy.GetConnection());
                        command1.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return true;
                    }
                    finally
                    {
                        proxy.Close();
                    }

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine();
                    Console.WriteLine("Updated successfully {0}ms", stopwatch.ElapsedMilliseconds);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    stopwatch.Stop();

                    return true;
	            }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}