﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlBulkApp
{
    public class DbDesign
    {
        //Customer

        public static string createTableCustomer = @"CREATE TABLE [dbo].[Customer](
	        [Id] [int] IDENTITY(1,1) NOT NULL,
	        [FirstName] [nvarchar](50) NOT NULL,
	        [LastName] [nvarchar](50) NOT NULL,
	        [FullName] [nvarchar](50) NULL,
	        [Address] [nvarchar](50) NOT NULL,
	        [City] [nvarchar](50) NOT NULL,
            CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([Id] ASC ))";

        public static string dropTableCustomer = @"DROP TABLE [dbo].[Customer]";

        //public static string alterTableCustomer = @"ALTER TABLE [dbo].[Customer] ADD [FullName] [nvarchar](50) NULL";

        // Order

        public static string createTableOrderProducts = @"CREATE TABLE [dbo].[OrderProduct](
	        [Id] [int] IDENTITY(1,1) NOT NULL,
            [Status] [int] DEFAULT 0 NOT NULL,
	        [CustomerId] [int] NOT NULL,
	        [OrderNr] [int] NOT NULL,
            [ProductName] [nvarchar](50) NOT NULL,
            [Qty] [int] NOT NULL,
	        [UnitPrice] [money] NOT NULL,
            CONSTRAINT [PK_OrderProduct] PRIMARY KEY CLUSTERED ([Id] ASC))";

        public static string createKeyOrderProducts = @"ALTER TABLE [dbo].[OrderProduct]  
            WITH NOCHECK ADD  CONSTRAINT [FK_OrderProduct_Customer] FOREIGN KEY([CustomerId])
            REFERENCES [dbo].[Customer] ([Id])";

        public static string dropKeyOrderProducts = @"ALTER TABLE [dbo].[OrderProduct] DROP CONSTRAINT [FK_OrderProduct_Customer]";

        public static string dropTableOrderProducts = @"DROP TABLE [dbo].[OrderProduct]";


        // Produkt

        public static string createTableProduct = @"CREATE TABLE [dbo].[Product](
	        [Id] [int] IDENTITY(1,1) NOT NULL,
	        [ProductName] [nvarchar](50) NOT NULL,
	        [Price] [money] NOT NULL,
        	[Description] [nvarchar](1000) NULL,
        	[ArticleId] [int] NULL,
            CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC))";

        public static string dropTableProduct = @"DROP TABLE [dbo].[Product]";

        // Invoice

        public static string createTableInvoice = @"CREATE TABLE [dbo].[Invoice](
	        [Id] [int] IDENTITY(1,1) NOT NULL,
	        [Created] [datetime] DEFAULT GETDATE() NOT NULL,
	        [InvoiceNr] [int] NOT NULL,
        	[OrderNr] [int] NOT NULL,
	        [FullName] [nvarchar](50) NOT NULL,
            [Address] [nvarchar](50) NOT NULL,
            [City] [nvarchar](50) NOT NULL,
            CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED ([Id] ASC))";

        public static string dropTableInvoice = @"DROP TABLE [dbo].[Invoice]";

        // Stored Procedures
        public static string createGetProductList = @"CREATE PROCEDURE [dbo].[GetProductList] AS SELECT * FROM Product";
        public static string dropGetProductList = @"DROP PROCEDURE [dbo].[GetProductList]";
        public static string createSpAddProduct = @"CREATE PROCEDURE [dbo].[spAddProduct] @ProductName nvarchar(50), @Price money, @Id int Out 
            AS BEGIN INSERT INTO Product (ProductName, Price) Values(@ProductName, @Price)
            SELECT @Id = SCOPE_IDENTITY() END";
        public static string dropSpAddProduct = @"DROP PROCEDURE [dbo].[spAddProduct]";


        //Views
        public static string createProductView = @"CREATE VIEW SelectAllProduct AS SELECT * FROM [dbo].[Product]";
        public static string dropProductView = @"DROP VIEW SelectAllProduct";

    }
}

 