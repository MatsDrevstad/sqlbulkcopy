﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SqlBulkApp
{
    class SqlTestProgram
    {
        public SqlTestProgram()
        {
            Console.WindowHeight = 60;
            Console.WindowWidth = 100;
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Välkommen till ADO.Net Test program");
            Console.ForegroundColor = ConsoleColor.Gray;

            bool loop = true;
            while (loop)
            {
                PrintMenuOptions();
                loop = HandleMenuOptions(loop);
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine("Välkommen tillbaka");
            Thread.Sleep(500);
        }

        public bool HandleMenuOptions(bool loop)
        {
            Console.Write("Input:\t");
            Console.ForegroundColor = ConsoleColor.Yellow;
            string input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Gray;

            if (input.Equals("1")) { DbGenerate.CreateTables(); }
            if (input.Equals("2")) { DbGenerate.CreateKeys(); }
            if (input.Equals("3")) { DbGenerate.DropKeys(); }
            if (input.Equals("4")) { DbGenerate.DropTables(); }
            if (input.Equals("5")) { DbGenerate.PopulateAllTables(); }
            if (input.Equals("6")) { DbGenerate.GenerateOrderProductTable(); }
            if (input.Equals("7")) { OperationTest.CreateInvoice(); }
            if (input.Equals("8")) { OperationTest.CreateBulkCopyTest(); }
            if (input.Equals("9")) { OperationTest.CreateSqlInsertTest(); }
            if (input.Equals("10")) { DbSP.ListSPProduct(); }
            if (input.Equals("11")) { DbGenerate.AlterCustomer(); }
            if (input.Equals("12")) { OperationTest.UpdatePriceDB(); }

            if (input.Equals("0")) { loop = false; }

            return loop;
        }

        private void PrintMenuOptions()
        {
            Console.ForegroundColor = ConsoleColor.Gray; 
            Console.WriteLine();
            Console.Write("Create\t");
            Console.WriteLine("[1] Create tables");
            Console.WriteLine("\t[2] Create key ORDERPRODUCT on CUSTOMER");
            Console.Write("Delete\t");
            Console.WriteLine("[3] Delete key ORDERPRODUCT on CUSTOMER");
            Console.WriteLine("\t[4] Delete tables");
            Console.Write("Pop\t");
            Console.WriteLine("[5] Populate PRODUCT and CUSTOMER");
            Console.WriteLine("\t[6] Generate ORDER");
            Console.WriteLine("\t[7] Generate INVOICE");
            Console.Write("Multi\t");
            Console.WriteLine("[8] Operation BULK");
            Console.WriteLine("\t[9] Operation SQL");
            Console.Write("StoPr\t");
            Console.WriteLine("[10] Run SP on Product");
            Console.Write("Alter\t");
            Console.WriteLine("[11] Alter column names CUSTOMER");
            Console.WriteLine("\t[12] Alter PRICE pending changes");
            Console.WriteLine();
            Console.WriteLine("\t[0] EXIT");
            Console.WriteLine();
        }
    }
}
