﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SqlBulkApp.Bill;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;

namespace SqlBulkApp
{
    class DbSP
    {
        public static void ListSPProduct()
        {
            var proxy = new DbProxy();
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var productList = new List<Product>();

            proxy.Open();

            SqlCommand cmd = new SqlCommand("GetProductList", proxy.GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlDataReader recordSet = cmd.ExecuteReader())
            {
                while (recordSet.Read())
                {
                    productList.Add(new Product
                    {
                        Id = (int) recordSet["Id"],
                        ProductName = (string) recordSet["ProductName"],
                        Price = (decimal) recordSet["Price"],
                        Description = (string)recordSet["Description"],
                        ArticleId = (int) recordSet["ArticleId"],
                    });
                }
            }
            proxy.Close();

            var elapsedMilliseconds = stopwatch.ElapsedMilliseconds;
            stopwatch.Stop();

            foreach (var item in productList)
            {
                Console.WriteLine(
                    item.Id + "\t" + 
                    item.ArticleId + "\t" + 
                    item.Price + "\t" + 
                    item.ProductName + "\t" + 
                    item.Description
                );
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Select successfully {0}ms", elapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
